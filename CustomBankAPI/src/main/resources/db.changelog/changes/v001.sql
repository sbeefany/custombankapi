create table Account
(
    id          UUID UNIQUE  NOT NULL,
    firstName   varchar(255) NOT NULL,
    secondName  varchar(255) NOT NULL,
    patronymic  varchar(255) NOT NULL,
    phoneNumber varchar(255) NOT NULL unique,
    age         int          not null,
    primary key (id)
);


create table BankAccount
(
    id       UUID UNIQUE  NOT NULL,
    bankName varchar(255) not null,
    balance  double       not null,
    primary key (id)
);

create table BANKACCOUNT_ACCOUNT
(
    account_id     UUID NOT NULL,
    bankaccount_id UUID NOT NULL,
    foreign key (account_id) references Account (id),
    foreign key (bankaccount_id) references BankAccount (id)
);

create table Card
(
    id             UUID UNIQUE NOT NULL,
    bank_account_id UUID        NOT NULL,
    foreign key (bank_account_id) references BankAccount (id),
    primary key (id)
);
