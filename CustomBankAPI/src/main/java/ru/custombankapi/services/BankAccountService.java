package ru.custombankapi.services;

import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;

import java.util.List;
import java.util.UUID;

public interface BankAccountService {
    /**
     * Метод для перевода с одного счета на другой
     *
     * @param bankAccountId1 счет с котогоро будет произведен перевод
     * @param bankAccountId2 счет на который будет сделан перевод
     * @param amount         размер перевода
     * @return
     * @throws BankAccountNotFoundException
     */
    BankAccount transferMoney(UUID bankAccountId1, UUID bankAccountId2, Double amount) throws BankAccountNotFoundException;

    /**
     * Метод для проверки баланса
     *
     * @param bankAccountId счет, чей баланс проверяется
     * @return возвращается значение баланса
     * @throws BankAccountNotFoundException
     */

    Double checkBalance(UUID bankAccountId) throws BankAccountNotFoundException;

    /**
     * Метод для изменения баланса
     *
     * @param bankAccountId счет, чей баланс будет изменен
     * @param amount        величина, на которую будет изменен счет
     * @return возврщается новое состояние счета
     * @throws BankAccountNotFoundException
     */
    BankAccount changeBalance(UUID bankAccountId, Double amount) throws BankAccountNotFoundException;

    /**
     * Метод для открытия нового счета
     *
     * @param accountId    идентификатор клиента
     * @param bankName     название банка
     * @param startBalance начальный баланс
     * @return возвращается созданный счет
     * @throws AccountNotFoundException
     */
    BankAccount openNewBankAccount(UUID accountId, String bankName, Double startBalance) throws AccountNotFoundException;

    /**
     * Метод для поиска счета по идентификатору
     *
     * @param bankAccountId идентифактор счета
     * @return возвращается инстанс счета
     * @throws BankAccountNotFoundException
     */
    BankAccount findBankAccount(UUID bankAccountId) throws BankAccountNotFoundException;

    /**
     * Метод для поиска всех счетов клиента
     *
     * @param accountId идентификатр клиента
     * @return возвращается список счетов
     * @throws AccountNotFoundException
     */
    List<BankAccount> findBankAccountsByAccountId(UUID accountId) throws AccountNotFoundException;

    /**
     * Метод для проверки существует ли счет по переданному идентификатору
     *
     * @param bankAccountId идентификатор счета
     * @return True-создан , False-не создан
     */
    Boolean checkAccountExists(UUID bankAccountId);

    /**
     * метод для перевода денег контрагенту
     *
     * @param myAccountId    идентификатор клиента
     * @param counterpartyId идентификатор контрагента
     * @param bankAccountId1 идентификатор банковского счета клиента
     * @param bankAccountId2 идентификатор банковского счета контрагента
     * @param amount         размер перевода
     * @return состояние банковского счета клиента
     * @throws AccountNotFoundException
     * @throws BankAccountNotFoundException
     */
    BankAccount transferMoneyToCounterparty(UUID myAccountId, UUID counterpartyId, UUID bankAccountId1, UUID bankAccountId2, Double amount) throws AccountNotFoundException, BankAccountNotFoundException;

    /**
     * метод для удаления счета
     * @param BankAccountId идентификатор счета
     * @throws BankAccountNotFoundException
     */
    void  deleteBankAccount(UUID BankAccountId) throws BankAccountNotFoundException;
}
