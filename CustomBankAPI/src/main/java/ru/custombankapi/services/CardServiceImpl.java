package ru.custombankapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.entities.Card;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.exceptions.CardNotFoundException;
import ru.custombankapi.repositories.CardRepository;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class CardServiceImpl implements CardService {

    private final CardRepository repository;
    private final BankAccountService bankAccountService;
    private final UserService accountService;

    @Autowired
    public CardServiceImpl(CardRepository repository, BankAccountService bankAccountRepository, UserService accountService) {
        this.repository = repository;
        this.bankAccountService = bankAccountRepository;
        this.accountService = accountService;
    }

    @Override
    public Card createNewCard(UUID bankAccountId) throws BankAccountNotFoundException {
        BankAccount bankAccount = bankAccountService.findBankAccount(bankAccountId);
        Card card = new Card(bankAccount);
        repository.saveNewCard(card);
        return card;
    }

    @Override
    public List<Card> getAllCardsByAccount(UUID accountId) throws AccountNotFoundException {
        boolean exists = accountService.checkAccountExistence(accountId);
        if (!exists)
            throw new AccountNotFoundException("Account with this id was not found!");
        return repository.findAllCards(accountId);
    }

    @Override
    public Card createNewCardWithoutBankAccount(UUID accountId, String bankName, Double startBalance) throws AccountNotFoundException {
        BankAccount newBankAccount = bankAccountService.openNewBankAccount(accountId, bankName, startBalance);
        Card newCard = new Card(newBankAccount);
        repository.saveNewCard(newCard);
        return newCard;
    }

    @Override
    public List<Card> getAllCardsByBankAccount(UUID bankAccountId) throws BankAccountNotFoundException {
        Boolean exists = bankAccountService.checkAccountExists(bankAccountId);
        if (!exists)
            throw new BankAccountNotFoundException("Bank account with this id was not found!");

        return repository.findAllCardsByBankAccount(bankAccountId);
    }

    @Override
    public void deleteCardById(UUID cardId) throws CardNotFoundException {
        Card card = repository.findCardById(cardId).orElseThrow(() -> new CardNotFoundException("Card with this id was not found!"));
        repository.deleteCard(card);
    }
}
