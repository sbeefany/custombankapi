package ru.custombankapi.services;

import ru.custombankapi.entities.Card;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.exceptions.CardNotFoundException;

import java.util.List;
import java.util.UUID;

public interface CardService {
    /**
     * Метод для создания новой карты к уже существующему счету
     * @param bankAccount идентификатор счета
     * @return возвращает новую карту
     * @throws BankAccountNotFoundException
     */
    Card createNewCard(UUID bankAccount) throws BankAccountNotFoundException;

    /**
     * Метод для поиска всех карт клиента
     * @param accountId идентификатор клиента
     * @return возвращает список кард
     * @throws AccountNotFoundException
     */
    List<Card> getAllCardsByAccount(UUID accountId) throws AccountNotFoundException;

    /**
     * Метод для создания новой карты с открытием нового счета
     * @param accountId идентификатор клиента
     * @param bankName название банка
     * @param startBalance стартовый баланс счета
     * @return возвращает новую карту
     * @throws AccountNotFoundException
     */
    Card createNewCardWithoutBankAccount(UUID accountId, String bankName, Double startBalance) throws AccountNotFoundException;

    /**
     * Метод для поиска всех карт привязанных к определенному счету
     * @param bankAccountId идентификатор счета
     * @return возвращает список карт
     * @throws BankAccountNotFoundException
     */
    List<Card> getAllCardsByBankAccount(UUID bankAccountId) throws BankAccountNotFoundException;

    void deleteCardById(UUID cardId) throws CardNotFoundException;
}
