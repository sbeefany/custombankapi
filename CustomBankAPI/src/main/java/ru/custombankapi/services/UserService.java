package ru.custombankapi.services;

import ru.custombankapi.entities.User;
import ru.custombankapi.exceptions.AccountNotFoundException;

import java.util.List;
import java.util.UUID;

public interface UserService {
    /**
     * Метод дл поиска учетной записи по идентификатору
     * @param accountId идентификатор
     * @return информацию о пользователе
     * @throws AccountNotFoundException выбрасывается если пользователь не найден
     */
    User findAccountByAccountId(UUID accountId) throws AccountNotFoundException;

    /**
     * метод для проверки существует учетная запись с данным идентификатором или нет
     * @param accountId идентификатор
     * @return true-существует, false-не существует
     */
    boolean checkAccountExistence(UUID accountId);

    /**
     * добавление контрагента
     * @param accountId идентификатор клиента
     * @param otherAccountId идентификатор контрагента
     * @return возвращается информация об учетной записи клиента
     * @throws AccountNotFoundException
     */
    User addCounterparty(UUID accountId, UUID otherAccountId) throws AccountNotFoundException;

    /**
     * метод для получения контрагентов клиента с переданным идентификаторм
     * @param accountId идентификатр клиента
     * @return список контрагентов
     * @throws AccountNotFoundException
     */
    List<User> showCounterparties(UUID accountId) throws AccountNotFoundException;

    /**
     * метод для проверки есть ли у данного клиента конкретный контрагент
     * @param accountId идентификатор клиента
     * @param otherAccountId идентификатор контрагента
     * @return true-существует, false-не существует
     * @throws AccountNotFoundException
     */
    Boolean checkCounterpartyExistence(UUID accountId,UUID otherAccountId) throws AccountNotFoundException;
}
