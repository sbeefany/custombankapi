package ru.custombankapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.custombankapi.entities.User;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.repositories.BankAccountRepository;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountRepository repository;
    private final UserService accountService;

    @Autowired
    public BankAccountServiceImpl(BankAccountRepository repository, UserService accountService) {
        this.repository = repository;
        this.accountService = accountService;
    }


    @Override
    public BankAccount transferMoney(UUID bankAccountId1, UUID bankAccountId2, Double amount) throws BankAccountNotFoundException {
        BankAccount bankAccount = changeBalance(bankAccountId1, -amount);
        changeBalance(bankAccountId2, amount);
        return bankAccount;
    }

    @Override
    public Double checkBalance(UUID bankAccountId) throws BankAccountNotFoundException {
        return repository.findBankAccount(bankAccountId).orElseThrow(
                () -> new BankAccountNotFoundException("Bank account with this id was not found!")
        ).getBalance();
    }


    @Override
    public BankAccount changeBalance(UUID bankAccountId, Double amount) throws BankAccountNotFoundException {
        BankAccount bankAccount = findBankAccount(bankAccountId);
        bankAccount.changeBalance(amount);
        return bankAccount;
    }


    @Override
    public BankAccount openNewBankAccount(UUID accountId, String bankName, Double startBalance) throws AccountNotFoundException {
        User account = accountService.findAccountByAccountId(accountId);
        BankAccount newBankAccount = new BankAccount(Collections.singletonList(account), bankName, startBalance);
        repository.saveBankAccount(newBankAccount);
        return newBankAccount;
    }


    @Override
    public BankAccount findBankAccount(UUID bankAccountId) throws BankAccountNotFoundException {
        return repository.findBankAccount(bankAccountId).orElseThrow(
                () -> new BankAccountNotFoundException("Bank account with this id was not found!")
        );
    }


    @Override
    public List<BankAccount> findBankAccountsByAccountId(UUID accountId) throws AccountNotFoundException {
        boolean exists = accountService.checkAccountExistence(accountId);
        if (!exists)
            throw new AccountNotFoundException("Account with this id was not found!");

        return repository.findAllBankAccountsByAccountId(accountId);
    }


    @Override
    public Boolean checkAccountExists(UUID bankAccountId) {
        return repository.findBankAccount(bankAccountId).isPresent();
    }

    @Override
    public BankAccount transferMoneyToCounterparty(UUID myAccountId, UUID counterpartyId, UUID bankAccountId1, UUID bankAccountId2, Double amount) throws AccountNotFoundException, BankAccountNotFoundException {
        Boolean existence = accountService.checkCounterpartyExistence(myAccountId, counterpartyId);
        if (!existence)
            throw new IllegalArgumentException("Account should contain counterparty,who receive money!");
        return transferMoney(bankAccountId1, bankAccountId2, amount);
    }

    @Override
    public void deleteBankAccount(UUID bankAccountId) throws BankAccountNotFoundException {
        BankAccount bankAccount = findBankAccount(bankAccountId);
        repository.deleteBankAccount(bankAccount);
    }
}
