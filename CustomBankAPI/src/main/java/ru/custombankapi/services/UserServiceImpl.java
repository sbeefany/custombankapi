package ru.custombankapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.custombankapi.entities.User;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository accountRepository;

    @Autowired
    public UserServiceImpl(UserRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public User findAccountByAccountId(UUID accountId) throws AccountNotFoundException {
        return accountRepository.findAccount(accountId).orElseThrow(
                () -> new AccountNotFoundException("Account with this id was not found!")
        );
    }

    @Override
    public boolean checkAccountExistence(UUID accountId) {
        return accountRepository.findAccount(accountId).isPresent();
    }

    @Override
    public User addCounterparty(UUID accountId, UUID otherAccountId) throws AccountNotFoundException, IllegalArgumentException {
        if (accountId.equals(otherAccountId))
            throw new IllegalArgumentException("Counterparty Id must be not equals account id");
        User account = findAccountByAccountId(accountId);
        User otherAccount = findAccountByAccountId(otherAccountId);
        account.addCounterparty(otherAccount);
        accountRepository.saveAccount(account);
        return account;
    }

    @Override
    public List<User> showCounterparties(UUID accountId) throws AccountNotFoundException {
        return new ArrayList<>(findAccountByAccountId(accountId).getCounterparties());
    }

    @Override
    public Boolean checkCounterpartyExistence(UUID firstAccount, UUID secondAccount) throws AccountNotFoundException {
        User account = findAccountByAccountId(firstAccount);
        User counterparty = findAccountByAccountId(secondAccount);
        return account.checkCounterpartyExistence(counterparty);
    }


}
