package ru.custombankapi.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Data
@Entity(name = "Account")
@Table(name = "ACCOUNT")
public class User {

    @Version
    private Long version;

    @Id
    @Column(name = "ID")
    private UUID accountId;
    @Column(name = "SECONDNAME")
    private String secondname;
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Column(name = "PATRONYMIC")
    private String patronymic;
    @Column(name = "AGE")
    private int age;
    @Column(name = "PHONENUMBER")
    private String phoneNumber;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "counterparties",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "counterparty_id")})
    private Set<User> counterparties;

    public User(String secondname, String firstname, String patronymic, int age, String phoneNumber, Set<User> counterparties) {
        this.secondname = secondname;
        this.firstname = firstname;
        this.patronymic = patronymic;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.counterparties = counterparties;
        accountId = UUID.randomUUID();
        version = 0L;
    }


    public void addCounterparty(User otherAccount) {
        counterparties.add(otherAccount);
    }

    public boolean checkCounterpartyExistence(User account) {
        return this.counterparties.contains(account);
    }

    public User() {
    }
}
