package ru.custombankapi.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity(name = "BankAccount")
@Table(name = "BANKACCOUNT")
public class BankAccount {
    @Version
    private Long version;

    @Id
    @Column(name = "ID")
    private UUID bankAccountId;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "BANKACCOUNT_ACCOUNT",
            joinColumns = {@JoinColumn(name = "bankaccount_id")},
            inverseJoinColumns = {@JoinColumn(name = "account_id")})
    private List<User> account;
    @Column(name = "BANKNAME")
    private String bankName;
    @Column(name = "BALANCE")
    private Double balance;


    public BankAccount(List<User> account, String bankName, Double startBalance) {
        this.account = account;
        this.bankName = bankName;
        balance = startBalance;
        bankAccountId = UUID.randomUUID();
        version = 0L;
    }

    public BankAccount() {

    }

    public void changeBalance(Double change) {
        this.balance += change;
    }
}
