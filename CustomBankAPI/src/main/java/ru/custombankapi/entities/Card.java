package ru.custombankapi.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity(name = "Card")
@Table(name = "CARD")
public class Card {
    @Version
    private Long version;

    @Id
    @Column(name = "ID")
    private UUID cardId;
    @ManyToOne
    private BankAccount bankAccount;

    public Card(BankAccount bankAccount) {
        cardId = UUID.randomUUID();
        this.bankAccount = bankAccount;
        version = 0L;
    }

    public Card() {
    }


}
