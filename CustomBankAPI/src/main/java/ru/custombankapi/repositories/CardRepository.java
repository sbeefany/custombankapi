package ru.custombankapi.repositories;

import ru.custombankapi.entities.Card;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CardRepository {
    /**
     * Метод для сохранения нвоой карты
     * @param newCard инстнас новой карты
     */
    void saveNewCard(Card newCard);

    /**
     * Метод для поиска всех карт клиента
     * @param accountId идентификатор клиента
     * @return возвращает список карт
     */
    List<Card> findAllCards(UUID accountId);

    /**
     * Метод для поиска всех карт привязанных к определенному счету
     * @param bankAccountId идентификатор счета
     * @return возвращает список карт
     */
    List<Card> findAllCardsByBankAccount(UUID bankAccountId);

    /**
     * метод для поиска карты по идентификатору
     * @param cardId идентификатор
     * @return информацию о карте
     */
    Optional<Card> findCardById(UUID cardId);

    void deleteCard(Card card);
}
