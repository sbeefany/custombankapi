package ru.custombankapi.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.custombankapi.entities.BankAccount;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class BankAccountRepositoryImpl implements BankAccountRepository {


    private final EntityManager entityManager;


    @Autowired
    public BankAccountRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public Optional<BankAccount> findBankAccount(UUID bankAccountId) {
        return Optional.ofNullable( entityManager.find(BankAccount.class, bankAccountId));
    }

    @Override
    public void saveBankAccount(BankAccount bankAccount) {
        entityManager.persist(bankAccount);
    }

    @Override
    public List<BankAccount> findAllBankAccountsByAccountId(UUID accountId) {
        Query query = entityManager.createNativeQuery(
                "select * from BANKACCOUNT " +
                        "join ACCOUNT_BANKACCOUNT BA on BANKACCOUNT.ID = BA.BANKACCOUNT_ID " +
                        "where ACCOUNT_ID = ?1"
        );
        query.setParameter(1, accountId.toString());
        return (List<BankAccount>) query.getResultList();
    }

    @Override
    public void deleteBankAccount(BankAccount bankAccount) {
        entityManager.remove(bankAccount);
    }
}
