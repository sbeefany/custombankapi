package ru.custombankapi.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.entities.Card;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class CardRepositoryImpl implements CardRepository {

    private final EntityManager entityManager;


    @Autowired
    public CardRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void saveNewCard(Card newCard) {

        entityManager.persist(newCard);
    }

    @Override
    public List<Card> findAllCards(UUID accountId) {
        List<Card> cards = entityManager.createQuery("select card from Card card").getResultList();
        return cards.stream().filter(
                card -> card.getBankAccount().getAccount().stream().anyMatch(
                        account -> account.getAccountId().equals(accountId)
                )
        ).collect(Collectors.toList());
    }

    @Override
    public List<Card> findAllCardsByBankAccount(UUID bankAccountId) {
        TypedQuery<Card> query = entityManager.createQuery("select card from Card card where card.bankAccount.bankAccountId=?1", Card.class);
        query.setParameter(1, bankAccountId);
        List<Card> cards = query.getResultList();

        return cards;
    }

    @Override
    public Optional<Card> findCardById(UUID cardId) {
        return Optional.ofNullable(entityManager.find(Card.class, cardId));
    }

    @Override
    public void deleteCard(Card card) {
        entityManager.remove(card);
    }
}
