package ru.custombankapi.repositories;

import ru.custombankapi.entities.BankAccount;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BankAccountRepository {
    /**
     * Метод для поиска счета по индентификатору
     * @param bankAccountId идентификатор счета
     * @return возвращается Optional карты
     */
    Optional<BankAccount> findBankAccount(UUID bankAccountId);

    /**
     * Метод для сохранения счета
     * @param bankAccount инстанс счета
     */
    void saveBankAccount(BankAccount bankAccount);

    /**
     * Метод для поиска всех счетов по идентификатору клиента
     * @param accountId идентификатор клиента
     * @return возвращает списк карт
     */
    List<BankAccount> findAllBankAccountsByAccountId(UUID accountId);

    void deleteBankAccount(BankAccount bankAccount);
}
