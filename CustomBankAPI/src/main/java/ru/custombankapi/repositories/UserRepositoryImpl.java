package ru.custombankapi.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import ru.custombankapi.entities.User;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
public class UserRepositoryImpl implements UserRepository {

    private final EntityManager entityManager;

    @Autowired
    public UserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Optional<User> findAccount(UUID accountId) {
        return Optional.ofNullable(entityManager.find(User.class, accountId));
    }

    @Override
    public void saveAccount(User newAccount) {
        entityManager.persist(newAccount);
    }
}
