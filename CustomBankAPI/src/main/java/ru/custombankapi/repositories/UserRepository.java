package ru.custombankapi.repositories;

import ru.custombankapi.entities.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository {

    /**
     * Метод для получения учетной записи по идентификатору
     * @param AccountId идентификатор учетной записи
     * @return Optional<Account>
     */
    Optional<User> findAccount(UUID AccountId);

    /**
     * метод сохранения пользователя
     * @param newAccount пользователь
     */
    void saveAccount(User newAccount);
}
