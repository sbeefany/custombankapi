package ru.custombankapi.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.custombankapi.entities.Card;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.exceptions.CardNotFoundException;
import ru.custombankapi.services.CardService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(("/api/cards"))
public class CardController {
    private final CardService cardService;

    private final Logger logger = LogManager.getLogger("project_logger");

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @PostMapping("/new")
    public ResponseEntity<Card> createNewCard(@RequestParam(name = "bankAccountId", required = false) UUID bankAccountId,
                                              @RequestParam(name = "accountId", required = false) UUID accountId,
                                              @RequestParam(name = "bankName", required = false, defaultValue = "ПАО СБЕРБАНК") String bankName,
                                              @RequestParam(name = "startBalance", required = false, defaultValue = "0") Double startBalance) throws BankAccountNotFoundException, AccountNotFoundException {
        logger.debug("post /api/cards/new");
        Card newCard = bankAccountId != null ? cardService.createNewCard(bankAccountId)
                : cardService.createNewCardWithoutBankAccount(accountId, bankName, startBalance);
        return ResponseEntity.ok(newCard);

    }

    @GetMapping("/all")
    public ResponseEntity<List<Card>> getAllCards(@RequestParam(name = "accountId", required = false) UUID accountId,
                                                  @RequestParam(name = "bankAccountId", required = false) UUID bankAccountId) throws AccountNotFoundException, BankAccountNotFoundException {
        logger.debug("get /api/cards/all");
        List<Card> cards = accountId != null ? cardService.getAllCardsByAccount(accountId) : cardService.getAllCardsByBankAccount(bankAccountId);
        return ResponseEntity.ok(cards);

    }

    @DeleteMapping("/{cardId}")
    public ResponseEntity deleteCards(@PathVariable(name = "cardId") UUID cardId) throws CardNotFoundException {
        logger.debug("delete /api/cards"+cardId);
        cardService.deleteCardById(cardId);
        return ResponseEntity.ok().build();
    }
}
