package ru.custombankapi.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.custombankapi.entities.User;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.services.UserService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/accounts")
public class UserController {

    private final UserService accountService;
    private final Logger logger = LogManager.getLogger("project_logger");

    @Autowired
    public UserController(UserService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<User> getAccountById(@PathVariable(name = "accountId") UUID accountId) throws AccountNotFoundException {

        logger.debug("get /api/accounts/"+accountId);
        User account = accountService.findAccountByAccountId(accountId);
        return ResponseEntity.ok(account);
    }

    @PostMapping("{accountId}/counterparties")
    public ResponseEntity<User> addCounterparty(@PathVariable(name = "accountId") UUID accountId,
                                                @RequestBody String counterpartyId) throws AccountNotFoundException {

        logger.debug("post /api/accounts/"+accountId+"/counterparties");
        User account = accountService.addCounterparty(accountId, UUID.fromString(counterpartyId));
        return ResponseEntity.ok(account);

    }

    @GetMapping("{accountId}/counterparties")
    public ResponseEntity<List<User>> addCounterparty(@PathVariable(name = "accountId") UUID accountId) throws AccountNotFoundException {
        logger.debug("get /api/accounts/"+accountId+"/counterparties");
        List<User> counterparties = accountService.showCounterparties(accountId);
        return ResponseEntity.ok(counterparties);

    }
}
