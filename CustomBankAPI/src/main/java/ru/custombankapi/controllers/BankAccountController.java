package ru.custombankapi.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.services.BankAccountService;

import java.util.UUID;

@RestController
@RequestMapping("/api/bankAccounts")
public class BankAccountController {

    private final BankAccountService bankAccountService;

    private final Logger logger = LogManager.getLogger("project_logger");

    public BankAccountController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @PostMapping("/{bankAccountId}/balance")
    public ResponseEntity<BankAccount> changeBalanceOfBankAccount(@PathVariable("bankAccountId") UUID bankAccountId,
                                                                  @RequestParam(name = "amount", required = false, defaultValue = "0") Double amount) throws BankAccountNotFoundException {
        logger.debug("post /api/bankAccounts/" + bankAccountId + "/balance");
        BankAccount bankAccount = bankAccountService.changeBalance(bankAccountId, amount);
        return ResponseEntity.ok(bankAccount);


    }

    @GetMapping("/{bankAccountId}/balance")
    public ResponseEntity<Double> checkBalanceOfBankAccount(@PathVariable("bankAccountId") UUID bankAccountId) throws BankAccountNotFoundException {
        logger.debug("get /api/bankAccounts/" + bankAccountId + "/balance");
        Double balance = bankAccountService.checkBalance(bankAccountId);
        return ResponseEntity.ok(balance);

    }

    @PostMapping("/{bankAccountId}/transactions/counterparties")
    public ResponseEntity<BankAccount> createTransactionWithCounterparty(@PathVariable(name = "bankAccountId") UUID bankAccountFrom,
                                                                         @RequestParam(name = "bankAccountId") UUID bankDestinationId,
                                                                         @RequestParam(name = "accountId") UUID accountId,
                                                                         @RequestParam(name = "counterpartyId") UUID counterpartyId,
                                                                         @RequestParam(name = "amount", required = false, defaultValue = "0") Double amount) throws BankAccountNotFoundException, AccountNotFoundException {

        logger.debug("post /api/bankAccounts/" + bankAccountFrom + "/transactions/counterparties");
        BankAccount bankAccount = bankAccountService.transferMoneyToCounterparty(accountId, counterpartyId, bankAccountFrom, bankDestinationId, amount);
        return ResponseEntity.ok(bankAccount);

    }

    @DeleteMapping("/{bankAccountId}")
    public ResponseEntity deleteBankAccount(@PathVariable(name = "bankAccountId") UUID bankAccountId) throws BankAccountNotFoundException {
        logger.debug("delete /api/bankAccounts/" + bankAccountId);
        bankAccountService.deleteBankAccount(bankAccountId);
        return ResponseEntity.ok().build();
    }


}
