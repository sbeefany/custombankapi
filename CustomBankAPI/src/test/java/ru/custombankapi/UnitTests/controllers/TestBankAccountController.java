package ru.custombankapi.UnitTests.controllers;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.custombankapi.controllers.BankAccountController;
import ru.custombankapi.entities.User;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.services.BankAccountService;

import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BankAccountController.class)
@ActiveProfiles({ "test" })
class TestBankAccountController {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BankAccountService bankAccountService;
    private UUID testBankAccountId;
    private BankAccount testBankAccount;

    @BeforeEach
    void setUp() {
        testBankAccountId = UUID.randomUUID();
        User account = new User("Пупкин", "Пупок", "Пупокиевич", 18,"+79998887766", new HashSet<>());
        testBankAccount = new BankAccount(Collections.singletonList(account), "ПАО СБЕРБАНК", 100D);
    }

    @Test
    public void testChangeBalanceOfBankAccount() throws Exception {

        when(bankAccountService.changeBalance(testBankAccountId,100D)).thenReturn(testBankAccount);
        mvc.perform(post("/api/bankAccounts/{bankAccountId}/balance",testBankAccountId.toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("amount","100"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance", Matchers.is(testBankAccount.getBalance())));
    }

    @Test
    public void testChangeBalanceOfBankAccountButThisBankAccountDoesNotExist() throws Exception {
        when(bankAccountService.changeBalance(testBankAccountId,100D))
                .thenThrow(new BankAccountNotFoundException("Bank account with this id was not found!"));
        mvc.perform(post("/api/bankAccounts/{bankAccountId}/balance",testBankAccountId.toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("amount","100"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("Bank account with this id was not found!")));
    }
    @Test
    public void testCheckBalanceOfBankAccount() throws Exception {

        when(bankAccountService.checkBalance(testBankAccountId)).thenReturn(100D);
        mvc.perform(MockMvcRequestBuilders.get("/api/bankAccounts/{bankAccountId}/balance",testBankAccountId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.is(100.0)));
    }

    @Test
    public void testCheckBalanceOfBankAccountButThisBankAccountDoesNotExist() throws Exception {

        when(bankAccountService.checkBalance(testBankAccountId)).thenThrow(new BankAccountNotFoundException("Bank account with this id was not found!"));
        mvc.perform(MockMvcRequestBuilders.get("/api/bankAccounts/{bankAccountId}/balance",testBankAccountId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("Bank account with this id was not found!")));
    }


}