package ru.custombankapi.UnitTests.controllers;


import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.custombankapi.controllers.CardController;
import ru.custombankapi.entities.User;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.entities.Card;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.services.CardService;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(CardController.class)
@ActiveProfiles({ "test" })
public class TestCardsController {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private CardService cardService;

    private UUID testBankAccountId;
    private Card testCard;
    private User account;
    private BankAccount testBankAccount;
    private UUID accountId;

    @BeforeEach
    public void init() {
        testBankAccountId = UUID.randomUUID();
        accountId = UUID.randomUUID();
        account = new User("Пупкин", "Пупок", "Пупокиевич", 18, "+79998887766", new HashSet<>());
        testBankAccount = new BankAccount(Collections.singletonList(account), "ПАО СБЕРБАНК", 1000d);
        testCard = new Card(testBankAccount);
    }

    @Test
    public void testCreateCard() throws Exception {
        when(cardService.createNewCard(testBankAccountId)).thenReturn(testCard);
        mvc.perform(post("/api/cards/new")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("bankAccountId", testBankAccountId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cardId", Matchers.is(testCard.getCardId().toString())));
    }

    @Test
    public void testCreateCardButBankAccountIdIsEmpty() throws Exception {
        when(cardService.createNewCardWithoutBankAccount(accountId, "ПАО СБЕРБАНК", 0d)).thenReturn(testCard);
        mvc.perform(post("/api/cards/new")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("accountId", accountId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cardId", Matchers.is(testCard.getCardId().toString())));
    }

    @Test
    public void testCreateCardButAllParamIsNull() throws Exception {
        when(cardService.createNewCardWithoutBankAccount(null, "ПАО СБЕРБАНК", 0d)).thenThrow(new AccountNotFoundException("Account with this id was not found!"));
        mvc.perform(post("/api/cards/new")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("Account with this id was not found!")));
    }

    @Test
    public void testGetAllCardsByAccountId() throws Exception {
        Card card1 = new Card(testBankAccount);
        Card card2 = new Card(testBankAccount);
        when(cardService.getAllCardsByAccount(accountId)).thenReturn(Arrays.asList(card1, card2));
        mvc.perform(MockMvcRequestBuilders.get("/api/cards/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("accountId", accountId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].cardId", Matchers.is(card1.getCardId().toString())));
    }

    @Test
    public void testGetAllCardsByBankAccountId() throws Exception {
        Card card1 = new Card(testBankAccount);
        Card card2 = new Card(testBankAccount);
        when(cardService.getAllCardsByBankAccount(testBankAccountId)).thenReturn(Arrays.asList(card1, card2));
        mvc.perform(MockMvcRequestBuilders.get("/api/cards/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("bankAccountId", testBankAccountId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].cardId", Matchers.is(card1.getCardId().toString())));
    }

    @Test
    public void testGetAllCardsButAllParamsIsNull() throws Exception {
        when(cardService.getAllCardsByBankAccount(null)).thenThrow(
                new BankAccountNotFoundException("Bank account with this id was not found!")
        );
        mvc.perform(MockMvcRequestBuilders.get("/api/cards/all")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("Bank account with this id was not found!")));
    }


}
