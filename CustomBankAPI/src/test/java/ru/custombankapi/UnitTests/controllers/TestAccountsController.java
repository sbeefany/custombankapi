package ru.custombankapi.UnitTests.controllers;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.custombankapi.controllers.UserController;
import ru.custombankapi.entities.User;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.services.UserService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@ActiveProfiles({ "test" })
public class TestAccountsController {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService accountService;
    private UUID accountId = UUID.randomUUID();
    private UUID counterpartyId = UUID.randomUUID();
    private User account;

    @BeforeEach
    void setUp() {
        account = new User("Пупкин", "Пупок", "Пупокиевич", 18, "+79998887766", new HashSet<>());
    }

    @Test
    public void testGetAccountById() throws Exception {
        when(accountService.findAccountByAccountId(accountId)).thenReturn(account);
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/{accountod}", accountId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accountId", Matchers.is(account.getAccountId().toString())));
    }

    @Test
    public void testGetAccountByIdButThrowsException() throws Exception {
        when(accountService.findAccountByAccountId(accountId)).thenThrow(new AccountNotFoundException("some message"));
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/{accountId}", accountId))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("some message")));
    }

    @Test
    public void testAddCounterparty() throws Exception {
        when(accountService.addCounterparty(accountId, counterpartyId)).thenReturn(account);
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/{accountId}/counterparties", accountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(counterpartyId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accountId", Matchers.is(account.getAccountId().toString())));
    }

    @Test
    public void testAddCounterpartyButThowException() throws Exception {
        when(accountService.addCounterparty(accountId, counterpartyId)).thenThrow(new AccountNotFoundException("some message"));
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/{accountId}/counterparties", accountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(counterpartyId.toString()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("some message")));
    }

    @Test
    public void testChekAllCounterpartiesById() throws Exception {
        when(accountService.showCounterparties(accountId)).thenReturn(Arrays.asList(account));
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/{accountId}/counterparties", accountId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)));
    }

    @Test
    public void testChekAllCounterpartiesByIdButThrowException() throws Exception {
        when(accountService.showCounterparties(accountId)).thenThrow(new AccountNotFoundException("some message"));
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/{accountId}/counterparties", accountId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("some message")));
    }
}
