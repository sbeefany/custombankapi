package ru.custombankapi.UnitTests.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.custombankapi.entities.User;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.entities.Card;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.repositories.CardRepository;
import ru.custombankapi.services.UserService;
import ru.custombankapi.services.BankAccountService;
import ru.custombankapi.services.CardService;
import ru.custombankapi.services.CardServiceImpl;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TestCardService {

    @Mock
    private CardRepository repository;
    @Mock
    private BankAccountService bankAccountService;
    @Mock
    private UserService accountService;
    private User account;
    private CardService cardService;
    private UUID bankAccountId;
    private UUID accountId;
    private BankAccount bankAccount;
    private Card card;

    @BeforeEach
    private void init() {
        cardService = new CardServiceImpl(repository, bankAccountService, accountService);
        bankAccountId = UUID.randomUUID();
        User account = new User("Пупкин", "Пупок", "Пупокиевич", 18,"+79998887766", new HashSet<>());
        bankAccount = new BankAccount(Collections.singletonList(account), "Пао Сбербанк", 0d);
        card = new Card(bankAccount);
    }

    @Test
    public void testCreateNewCard() throws BankAccountNotFoundException {
        when(bankAccountService.findBankAccount(bankAccountId)).thenReturn(bankAccount);

        Card newCard = cardService.createNewCard(bankAccountId);

        verify(repository, times(1)).saveNewCard(newCard);
        assertEquals(bankAccount, newCard.getBankAccount());
    }

    @Test
    public void testCreateNewCardWithNullAccount() throws BankAccountNotFoundException {
        when(bankAccountService.findBankAccount(null)).thenThrow(new BankAccountNotFoundException("Bank account with this id was not found!"));

        BankAccountNotFoundException bankAccountNotFoundException = assertThrows(BankAccountNotFoundException.class, () -> {
            cardService.createNewCard(null);
        });

        assertNotNull(bankAccountNotFoundException);
        assertEquals("Bank account with this id was not found!", bankAccountNotFoundException.getMessage());
    }

    @Test
    public void testCreateNewCardButAccountIsNotExist() throws BankAccountNotFoundException {
        when(bankAccountService.findBankAccount(bankAccountId)).thenThrow(new BankAccountNotFoundException("Bank account with this id was not found!"));

        BankAccountNotFoundException bankAccountNotFoundException = assertThrows(BankAccountNotFoundException.class, () -> {
            cardService.createNewCard(bankAccountId);
        });

        assertNotNull(bankAccountNotFoundException);
        assertEquals("Bank account with this id was not found!", bankAccountNotFoundException.getMessage());
    }

    @Test
    public void testShowAllCardsByPersonAccount() throws AccountNotFoundException {
        when(accountService.checkAccountExistence(accountId)).thenReturn(true);
        when(repository.findAllCards(accountId)).thenReturn(Collections.singletonList(card));

        List<Card> resultList = cardService.getAllCardsByAccount(accountId);
        assertEquals(1, resultList.size());
        assertEquals(Collections.singletonList(card), resultList);
    }

    @Test
    public void testShowAllCardsByPersonAccountButAccountDoesNotExist() throws AccountNotFoundException {
        when(accountService.checkAccountExistence(accountId)).thenReturn(false);

        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            List<Card> resultList = cardService.getAllCardsByAccount(accountId);
        });
        assertEquals("Account with this id was not found!",exception.getMessage());

    }

    @Test
    public void testShowAllCardsByBankAccount() throws BankAccountNotFoundException {
        when(bankAccountService.checkAccountExists(bankAccountId)).thenReturn(true);
        when(repository.findAllCardsByBankAccount(bankAccountId)).thenReturn(Collections.singletonList(card));

        List<Card> resultList = cardService.getAllCardsByBankAccount(bankAccountId);
        assertEquals(1, resultList.size());
        assertEquals(Collections.singletonList(card), resultList);
    }

    @Test
    public void testShowAllCardsByBankAccountButAccountDoesNotExist()  {
        when(bankAccountService.checkAccountExists(bankAccountId)).thenReturn(false);

        BankAccountNotFoundException exception = assertThrows(BankAccountNotFoundException.class, () -> {
            List<Card> resultList = cardService.getAllCardsByBankAccount(bankAccountId);
        });
        assertEquals("Bank account with this id was not found!",exception.getMessage());

    }


    @Test
    public void testCreateCardWithoutBankAccount() throws AccountNotFoundException {
        when(bankAccountService.openNewBankAccount(accountId, "ПАО СБЕРБАНК", 0d)).thenReturn(bankAccount);
        cardService.createNewCardWithoutBankAccount(accountId, "ПАО СБЕРБАНК", 0d);
        verify(repository, times(1)).saveNewCard(Mockito.any(Card.class));
    }
}
