package ru.custombankapi.UnitTests.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.custombankapi.entities.User;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.exceptions.BankAccountNotFoundException;
import ru.custombankapi.repositories.BankAccountRepository;
import ru.custombankapi.services.UserService;
import ru.custombankapi.services.BankAccountService;
import ru.custombankapi.services.BankAccountServiceImpl;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class TestBankAccountService {

    @Mock
    private BankAccountRepository bankAccountRepository;
    @Mock
    private UserService accountService;
    private BankAccountService service;

    private User account;
    private BankAccount firstBankAccount;
    private BankAccount otherBankAccount;
    private final UUID bankAccountId1 = UUID.randomUUID();
    private final UUID bankAccountId2 = UUID.randomUUID();
    private final UUID accountId = UUID.randomUUID();
    private final UUID myAccountId = UUID.randomUUID();
    private final UUID counterpartyId = UUID.randomUUID();


    @BeforeEach
    public void init() {
        service = new BankAccountServiceImpl(bankAccountRepository, accountService);
        account = new User("Пупкин", "Пупок", "Пупокиевич", 18, "+79998887766", new HashSet<>());
        firstBankAccount = new BankAccount(Collections.singletonList(account), "ПАО СБЕРБАНК", 1000d);
        otherBankAccount = new BankAccount(Collections.singletonList(account), "ПАО СБЕРБАНК", 500d);
    }

    @Test
    public void testTransferMoneyFromOneAccountToAnotherOne() throws BankAccountNotFoundException {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.of(firstBankAccount));
        when(bankAccountRepository.findBankAccount(bankAccountId2)).thenReturn(Optional.of(otherBankAccount));
        Double startBalance = firstBankAccount.getBalance();
        Double startBalance2 = otherBankAccount.getBalance();


        service.transferMoney(bankAccountId1, bankAccountId2, 500d);


        assertEquals(startBalance2 + 500d, otherBankAccount.getBalance());
        assertEquals(startBalance - 500d, firstBankAccount.getBalance());
    }

    @Test
    public void testTransferMoneyToCounterparty() throws AccountNotFoundException, BankAccountNotFoundException {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.of(firstBankAccount));
        when(bankAccountRepository.findBankAccount(bankAccountId2)).thenReturn(Optional.of(otherBankAccount));
        when(accountService.checkCounterpartyExistence(myAccountId, counterpartyId)).thenReturn(true);
        Double startBalance = firstBankAccount.getBalance();
        Double startBalance2 = otherBankAccount.getBalance();

        service.transferMoneyToCounterparty(myAccountId, counterpartyId, bankAccountId1, bankAccountId2, 500d);

        assertEquals(startBalance2 + 500d, otherBankAccount.getBalance());
        assertEquals(startBalance - 500d, firstBankAccount.getBalance());
    }

    @Test
    public void testTransferMoneyToCounterpartyButAccountDoesNotHaveThisCounterparty() throws AccountNotFoundException, BankAccountNotFoundException {

        when(accountService.checkCounterpartyExistence(myAccountId, counterpartyId)).thenReturn(false);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            service.transferMoneyToCounterparty(myAccountId, counterpartyId, bankAccountId1, bankAccountId2, 500d);
        });
        assertEquals("Account should contain counterparty,who receive money!", exception.getMessage());
    }

    @Test
    public void testTransferMoneyFromOneAccountToAnotherOneButTheFirstDoesNotExist() throws BankAccountNotFoundException {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.empty());


        BankAccountNotFoundException exception = assertThrows(BankAccountNotFoundException.class, () -> {
            service.transferMoney(bankAccountId1, bankAccountId2, 500d);
        });

        assertEquals("Bank account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testTransferMoneyFromOneAccountToAnotherOneButTheSecondDoesNotExist() throws BankAccountNotFoundException {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.of(firstBankAccount));
        when(bankAccountRepository.findBankAccount(bankAccountId2)).thenReturn(Optional.empty());


        BankAccountNotFoundException exception = assertThrows(BankAccountNotFoundException.class, () -> {
            service.transferMoney(bankAccountId1, bankAccountId2, 500d);
        });

        assertEquals("Bank account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testTransferMoneyFromOneAccountToAnotherOneButOneOfTheseIsNull() throws BankAccountNotFoundException {

        BankAccountNotFoundException exception = assertThrows(BankAccountNotFoundException.class, () -> {
            service.transferMoney(null, bankAccountId2, 500d);
        });

        assertEquals("Bank account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testCheckBalance() throws BankAccountNotFoundException {

        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.of(firstBankAccount));

        assertEquals(firstBankAccount.getBalance(), service.checkBalance(bankAccountId1));
    }

    @Test
    public void testCheckBalanceButAccountDoesNotExist() throws BankAccountNotFoundException {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.empty());

        BankAccountNotFoundException exception = assertThrows(BankAccountNotFoundException.class, () -> {
            service.checkBalance(bankAccountId1);
        });

        assertEquals("Bank account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testCheckBalanceFromNull() throws BankAccountNotFoundException {

        BankAccountNotFoundException exception = assertThrows(BankAccountNotFoundException.class, () -> {
            service.checkBalance(null);
        });

        assertEquals("Bank account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testChangeBalanceByBankAccountId() throws BankAccountNotFoundException {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.of(firstBankAccount));

        BankAccount result = service.changeBalance(bankAccountId1, 500d);

        firstBankAccount.changeBalance(500d);

        assertEquals(firstBankAccount, result);
    }

    @Test
    public void testFindBankAccountsByAccountId() throws AccountNotFoundException {
        when(accountService.checkAccountExistence(accountId)).thenReturn(true);
        when(bankAccountRepository.findAllBankAccountsByAccountId(accountId)).thenReturn(Arrays.asList(firstBankAccount, otherBankAccount));

        List<BankAccount> bankAccounts = service.findBankAccountsByAccountId(accountId);

        assertEquals(Arrays.asList(firstBankAccount, otherBankAccount), bankAccounts);
    }

    @Test
    public void testFindBankAccountsByAccountIdButAccountDoesNotExist() {
        when(accountService.checkAccountExistence(accountId)).thenReturn(false);

        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            service.findBankAccountsByAccountId(accountId);
        });

        assertEquals("Account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testFindBankAccountByBankAccountId() throws BankAccountNotFoundException {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.of(firstBankAccount));

        BankAccount bankAccount = service.findBankAccount(bankAccountId1);

        assertEquals(firstBankAccount, bankAccount);
    }

    @Test
    public void testFindBankAccountByBankAccountIdButBankAccountDoesNotExist() {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.empty());

        BankAccountNotFoundException exception = assertThrows(BankAccountNotFoundException.class, () -> {
            service.findBankAccount(bankAccountId1);
        });

        assertEquals("Bank account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testOpenNewBankAccountByAccountId() throws AccountNotFoundException {

        when(accountService.findAccountByAccountId(accountId)).thenReturn(account);

        BankAccount newBankAccount = service.openNewBankAccount(accountId, "ПАО СБЕРБАНК", 0d);

        verify(bankAccountRepository, times(1)).saveBankAccount(newBankAccount);
    }

    @Test
    public void testOpenNewBankAccountByAccountIdButAccountDoesNotExist() throws AccountNotFoundException {
        when(accountService.findAccountByAccountId(accountId)).thenThrow(new AccountNotFoundException("Account with this id was not found!"));
        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            BankAccount newBankAccount = service.openNewBankAccount(accountId, "ПАО СБЕРБАНК", 0d);
        });
        assertEquals("Account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testCheckBankAccountExistByBankAccountId() {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.of(firstBankAccount));

        assertTrue(service.checkAccountExists(bankAccountId1));
    }

    @Test
    public void testCheckBankAccountExistByBankAccountIdButThisAccountDoesNotExist() {
        when(bankAccountRepository.findBankAccount(bankAccountId1)).thenReturn(Optional.empty());

        assertFalse(service.checkAccountExists(bankAccountId1));
    }


}
