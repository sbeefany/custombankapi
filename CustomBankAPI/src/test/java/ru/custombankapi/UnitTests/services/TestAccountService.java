package ru.custombankapi.UnitTests.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.custombankapi.entities.User;
import ru.custombankapi.exceptions.AccountNotFoundException;
import ru.custombankapi.repositories.UserRepository;
import ru.custombankapi.services.UserService;
import ru.custombankapi.services.UserServiceImpl;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TestAccountService {

    @Mock
    private UserRepository accountRepository;

    private UserService accountService;

    private final UUID accountId = UUID.randomUUID();
    private final UUID otherAccountId = UUID.randomUUID();
    private final UUID bankAccountId = UUID.randomUUID();
    private final UUID otherBankAccountId = UUID.randomUUID();
    private User account;
    private User account2;

    @BeforeEach
    void setUp() {
        account = new User("Пупкин", "Пупок", "Пупокиевич", 18, "+79998887766", new HashSet<>());
        account2 = new User("Пупкин", "Пупок", "Пупокиевич", 18, "+79998887766", new HashSet<>(Arrays.asList(account)));
        accountService = new UserServiceImpl(accountRepository);
    }

    @Test
    public void testCheckAccountExist() {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.of(account));
        assertTrue(accountService.checkAccountExistence(accountId));
    }

    @Test
    public void testCheckAccountExistButAccountDoesNotExist() {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.empty());
        assertFalse(accountService.checkAccountExistence(accountId));
    }

    @Test
    public void testAddNewCounterparty() throws AccountNotFoundException {
        int sizeBefore = account.getCounterparties().size();
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.of(account));
        when(accountRepository.findAccount(otherAccountId)).thenReturn(Optional.of(account));
        User newAccount = accountService.addCounterparty(accountId, otherAccountId);

        assertEquals(sizeBefore + 1, newAccount.getCounterparties().size());
        verify(accountRepository, times(1)).saveAccount(newAccount);
    }

    @Test
    public void testAddNewCounterpartyButCounterpartysIdEqualsAccountId() throws AccountNotFoundException {

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            accountService.addCounterparty(accountId, accountId);
        });

        assertEquals("Counterparty Id must be not equals account id", exception.getMessage());

    }

    @Test
    public void testAddNewCounterpartyButAccountDoesNotExist() throws AccountNotFoundException {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.empty());


        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.addCounterparty(accountId, otherAccountId);
        });

        assertEquals("Account with this id was not found!", exception.getMessage());

    }

    @Test
    public void testAddNewCounterpartyButCounterpartysAccountDoesNotExist() throws AccountNotFoundException {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.of(account));
        when(accountRepository.findAccount(otherAccountId)).thenReturn(Optional.empty());

        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.addCounterparty(accountId, otherAccountId);
        });

        assertEquals("Account with this id was not found!", exception.getMessage());

    }

    @Test
    public void testShowAllCounterpartiesOfAccount() throws AccountNotFoundException {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.of(account2));

        assertEquals(account2.getCounterparties().size(), accountService.showCounterparties(accountId).size());
    }

    @Test
    public void testShowAllCounterpartiesOfAccountButAccountDoesNotExist() {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.empty());
        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.showCounterparties(accountId);
        });

        assertEquals("Account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testCheckCounterpartyExistence() throws AccountNotFoundException {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.of(account));
        when(accountRepository.findAccount(otherAccountId)).thenReturn(Optional.of(account2));
        Boolean existence= accountService.checkCounterpartyExistence(otherAccountId, accountId);
        assertEquals(true,existence);
    }
    @Test
    public void testCheckCounterpartyExistenceButAccountDoesNotExist() throws AccountNotFoundException {
        when(accountRepository.findAccount(otherAccountId)).thenReturn(Optional.empty());

        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            Boolean existence= accountService.checkCounterpartyExistence(otherAccountId, accountId);
        });

        assertEquals("Account with this id was not found!", exception.getMessage());
    }

    @Test
    public void testCheckCounterpartyExistenceButAccountDoesNotHaveThisCounterparty() throws AccountNotFoundException {
        when(accountRepository.findAccount(accountId)).thenReturn(Optional.of(account));
        when(accountRepository.findAccount(otherAccountId)).thenReturn(Optional.of(account2));
        Boolean existence= accountService.checkCounterpartyExistence(accountId, otherAccountId);
        assertEquals(false,existence);
    }

}
