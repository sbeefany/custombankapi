package ru.custombankapi.IntegrationTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.custombankapi.entities.Card;
import ru.custombankapi.services.CardService;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
public class IntegrationTestCardApi {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private CardService cardService;

    private UUID bankAccountId;
    private UUID accountId;


    @BeforeEach
    void setUp() {
        bankAccountId = UUID.fromString("29d46b6a-fa32-4057-8f89-b819bc797943");
        accountId = UUID.fromString("efbbbf65-3863-3163-3232-312d66343238");
    }

    @Test
    public void emptyIntegrationTest() {
        assertTrue(true);
    }

    @Test
    public void integrationTestCreateNewCard() throws Exception {
        int beforeCards = cardService.getAllCardsByAccount(accountId).size();
        mvc.perform(post("/api/cards/new")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("bankAccountId", bankAccountId.toString()))
                .andExpect(status().isOk());
        assertEquals(beforeCards + 1, cardService.getAllCardsByAccount(accountId).size());
    }

    @Test
    public void integrationTestCreateNewCardButBankAccountDoesNotExist() throws Exception {
        mvc.perform(post("/api/cards/new")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("bankAccountId", UUID.randomUUID().toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void integrationTestGetAllCardsByAccount() throws Exception {
        List<Card> allCards = cardService.getAllCardsByAccount(accountId);

        MockHttpServletResponse response = mvc.perform(get("/api/cards/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("accountId", this.accountId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse();
        String content = response.getContentAsString(StandardCharsets.UTF_8);
        String string = new ObjectMapper().writeValueAsString(allCards);
        assertEquals(string, content);
    }

    @Test
    public void integrationTestGetAllCardsByBankAccount() throws Exception {
        List<Card> allCards = cardService.getAllCardsByBankAccount(bankAccountId);

        MockHttpServletResponse response = mvc.perform(get("/api/cards/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("bankAccountId", this.bankAccountId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse();
        String content = response.getContentAsString(StandardCharsets.UTF_8);
        String string = new ObjectMapper().writeValueAsString(allCards);
        assertEquals(string, content);
    }

    @Test
    public void integrationTestGetAllCardsByBankAccountButBankAccountDoesNotExist() throws Exception {

        mvc.perform(get("/api/cards/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("bankAccountId", UUID.randomUUID().toString()))
                .andExpect(status().isBadRequest());

    }

}

