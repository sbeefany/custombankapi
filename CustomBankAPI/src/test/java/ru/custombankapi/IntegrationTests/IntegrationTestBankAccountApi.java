package ru.custombankapi.IntegrationTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.custombankapi.entities.BankAccount;
import ru.custombankapi.repositories.BankAccountRepository;
import ru.custombankapi.services.BankAccountService;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({ "test" })
public class IntegrationTestBankAccountApi {

    @Autowired
    private MockMvc mvc;
    private UUID bankAccountId;
    private UUID bankAccountDestinationId;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @BeforeEach
    void setUp() {
        bankAccountId = UUID.fromString("29d46b6a-fa32-4057-8f89-b819bc797943");
        bankAccountDestinationId = UUID.fromString("29d46b6a-fa32-4057-8f89-b819bc797944");
    }

    @Test
    public void integrationTestChangeBalanceOfBankAccount() throws Exception {
        Double balance = bankAccountRepository.findBankAccount(bankAccountId).get().getBalance();

        MockHttpServletResponse response = mvc.perform(post("/api/bankAccounts/{bankAccountId}/balance", bankAccountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("amount", "100.0")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse();
        String content = response.getContentAsString(StandardCharsets.UTF_8);
        BankAccount newBankAccount = new ObjectMapper().readValue(content, BankAccount.class);
        assertEquals(balance + 100, newBankAccount.getBalance());
    }

    @Test
    public void integrationTestChangeBalanceOfBankAccountBuBankAccountDoesNotExist() throws Exception {
        mvc.perform(post("/api/bankAccounts/{bankAccountId}/balance", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("amount", "100.0")
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void integrationTestChangeBalanceOfBankAccountWithoutAmount() throws Exception {
        Double balance = bankAccountRepository.findBankAccount(bankAccountId).get().getBalance();

        MockHttpServletResponse response = mvc.perform(post("/api/bankAccounts/{bankAccountId}/balance", bankAccountId)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse();
        String content = response.getContentAsString(StandardCharsets.UTF_8);
        BankAccount newBankAccount = new ObjectMapper().readValue(content, BankAccount.class);
        assertEquals(balance, newBankAccount.getBalance());
    }

    @Test
    public void integrationTestCheckBalanceOfBankAccount() throws Exception {
        Double balance = bankAccountRepository.findBankAccount(bankAccountId).get().getBalance();

        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.get("/api/bankAccounts/{bankAccountId}/balance", bankAccountId)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse();
        String content = response.getContentAsString();
        assertEquals(balance, Double.valueOf(content));
    }

    @Test
    public void integrationTestCheckBalanceOfBankAccountButBankAccountDoesNotExist() throws Exception {

        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.get("/api/bankAccounts/{bankAccountId}/balance", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest())
                .andReturn().getResponse();
        String content = response.getContentAsString();
        assertEquals("Bank account with this id was not found!", content);
    }
    @Test
    public void integrationTestTransferToCounterparty() throws Exception {

        Double balanceBefore = bankAccountRepository.findBankAccount(bankAccountId).get().getBalance();
        String content = mvc.perform(post("/api/bankAccounts/{bankAccountId}/transactions/counterparties", bankAccountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("accountId", "efbbbf65-3863-3163-3232-312d66343238")
                        .param("counterpartyId","efbbbf65-3863-3163-3232-312d66343256")
                        .param("bankAccountId", bankAccountDestinationId.toString())
                        .param("amount", "100.0")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        BankAccount bankAccount1 = bankAccountRepository.findBankAccount(bankAccountId).get();
        BankAccount bankAccount2 = bankAccountRepository.findBankAccount(bankAccountDestinationId).get();
        Double balanceAfter = bankAccount1.getBalance();
        BankAccount newBankAccount = new ObjectMapper().readValue(content, BankAccount.class);
        assertEquals(balanceAfter, newBankAccount.getBalance());
        assertEquals(balanceAfter+100, balanceBefore);


        bankAccount1.changeBalance(100D);
        bankAccount2.changeBalance(-100D);

    }

}
