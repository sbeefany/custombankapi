package ru.custombankapi.IntegrationTests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.custombankapi.entities.User;
import ru.custombankapi.repositories.UserRepository;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({ "test" })
public class IntegrationTestAccountApi {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private UserRepository accountRepository;

    private UUID accountId = UUID.fromString("efbbbf65-3863-3163-3232-312d66343238");
    private UUID counterpartyId = UUID.fromString("efbbbf65-3863-3163-3232-312d66343256");


    @Test
    public void testFindAccountById() throws Exception {
        User account = accountRepository.findAccount(accountId).get();
        String content = mvc.perform(MockMvcRequestBuilders.get("/api/accounts/{accountod}", accountId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(account, new ObjectMapper().readValue(content, User.class));
    }

    @Test
    public void testFindAccountByIdButThisAccountDoesNotExist() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/{accountId}", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddCounterparty() throws Exception {
        String content = mvc.perform(MockMvcRequestBuilders.post("/api/accounts/{accountId}/counterparties", accountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(counterpartyId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        User accountAfter = accountRepository.findAccount(accountId).get();

        assertEquals(accountAfter,new ObjectMapper().readValue(content, User.class));

    }

    @Test
    public void testAddCounterpartyButCounterartyDoesNotExist() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/{accountId}/counterparties", accountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(UUID.randomUUID().toString()))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void testShowAllCounterpartyByAccountId() throws Exception {
        String content = mvc.perform(MockMvcRequestBuilders.get("/api/accounts/{accountId}/counterparties", accountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(counterpartyId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        User accountAfter = accountRepository.findAccount(accountId).get();

        assertIterableEquals(accountAfter.getCounterparties(),new ObjectMapper().readValue(content,new TypeReference<List<User>>(){}));
    }
}
