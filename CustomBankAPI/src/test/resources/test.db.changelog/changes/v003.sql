create table counterparties
(
    account_id      uuid not null,
    counterparty_id uuid not null,
    foreign key (account_id) REFERENCES ACCOUNT(ID),
    foreign key (counterparty_id) REFERENCES ACCOUNT(ID)
);