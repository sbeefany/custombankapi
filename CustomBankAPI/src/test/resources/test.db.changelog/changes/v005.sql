ALTER TABLE Account
    ADD version long not null default 0 check (version >= 0);
ALTER TABLE BankAccount
    ADD version long not null default 0 check (version >= 0);
ALTER TABLE card
    ADD version long not null default 0 check (version >= 0);